var express = require('express');
var router = express.Router();
var db = require('../database/db');
var Schema = require('../schema/Schema');
var action = require('../action/action');
var wsServer = require('../websocket/server');
const { generateID } = require('../action/action');

/* GET home page. */
router.get('/', async function (req, res, next) {
  var boardResult = await db.model('Board', Schema.columnListSchema).findOne().exec();
  if (boardResult === null) {
    var result = await InitDatabase();

    res.status(200);
    res.json(result);

    wsServer.InitializeServer(result.wsChannel);
  } else {
    var column = await db.model('Column', Schema.columnSchema).find().exec();
    var card = await db.model('Card', Schema.cardSchema).find().exec();

    res.status(200);
    res.json({
      list: boardResult.list,
      wsChannel: boardResult.wsChannel,
      columnList: column,
      cardList: card
    });
    
    wsServer.InitializeServer(boardResult.wsChannel);
  }
});

async function InitDatabase() {
  var boardModel = db.model('Board', Schema.columnListSchema);
  var columnModel = db.model('Column', Schema.columnSchema);
  var cardModel = db.model('Card', Schema.cardSchema);

  var newCard = new cardModel({ID: generateID(12), title: 'Empty card'});
  await newCard.save();

  var newColumn = new columnModel({ ID: generateID(12), title: 'Welcome', cardList: [newCard.ID]})
  await newColumn.save();

  var newBoard = new boardModel({ list: [newColumn.ID], wsChannel: action.generateID(8)})
  await newBoard.save();

  return {
    list: [newColumn.ID],
    wsChannel: newBoard.wsChannel,
    columnList: [newColumn],
    cardList: [newCard]
  }
}

module.exports = router;
