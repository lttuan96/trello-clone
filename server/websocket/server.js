require('dotenv').config();
var WebSocket = require('ws');
var db = require('../database/db');
var Schema = require('../schema/Schema');

let isServerRunning = false;

function InitializeServer(messagePath) {
  if (isServerRunning) return;

  let clientList = [];
  console.log("Websocket server is running");
  const wss = new WebSocket.Server({ port: process.env.WS_PORT, path: '/' + messagePath});
  isServerRunning = true;

  wss.on('connection', function onConnection(ws) {
    console.log('New connection');
    clientList.push(ws)

    ws.on('message', async function onMessage(textMessage) {
      const boardModel = db.model('Board', Schema.columnListSchema);
      const columnModel = db.model('Column', Schema.columnSchema);
      const cardModel = db.model('Card', Schema.cardSchema);
      const board = await boardModel.findOne().exec();

      const message = JSON.parse(textMessage);

      console.log(textMessage);

      switch (message.type) {
        case 'ADD_COLUMN': {
          const { columnID, columnTitle } = message.data;

          board.list = [
            ...board.list,
            columnID
          ];

          await board.save();

          const newColumn = new columnModel({
            ID: columnID,
            title: columnTitle
          })

          await newColumn.save();
          break;
        }
      
        case 'MOVE_COLUMN': {
          const { srcPos, destPos } = message.data;

          const [moveColumnID] = board.list.splice(srcPos, 1);
          board.list.splice(destPos, 0, moveColumnID);

          await board.save();
          break;
        }
         
        case 'DELETE_COLUMN': {
          const { columnID } = message.data;

          const filteredList = board.list.filter(id => id !== columnID);
          board.list = filteredList;

          await board.save();

          await columnModel.findOneAndDelete({ID: columnID})
          break;
        }
         
        case 'RENAME_COLUMN': {
          const { columnID, columnNewTitle } = message.data;

          await columnModel.findOneAndUpdate({ ID: columnID }, { title: columnNewTitle });
          break;
        }
         
        case 'ADD_CARD': {
          const { columnID, cardID, cardTitle, label = 0, description = '', deadline = '' } = message.data;

          let targetColumn = await columnModel.findOne({ ID: columnID }).exec();
          targetColumn.cardList = [
            ...targetColumn.cardList,
            cardID
          ];
          await targetColumn.save();

          const newCard = new cardModel({ ID: cardID, title: cardTitle, label: label, description: description, deadline: deadline });
          await newCard.save();
          break;
        }
        case 'MOVE_CARD': {
          const { srcCol, destCol, srcPos, destPos } = message.data;

          if (srcCol === destCol) {
            let targetColumn = await columnModel.findOne({ ID: srcCol }).exec();

            const [movedCard] = targetColumn.cardList.splice(srcPos, 1);

            if (srcPos < destPos) {
              targetColumn.cardList.splice(destPos - 1, 0, movedCard);
            } else {
              targetColumn.cardList.splice(destPos, 0, movedCard);
            }

            await targetColumn.save();
          } else {
            let srcColumn = await columnModel.findOne({ ID: srcCol }).exec();
            let destColumn = await columnModel.findOne({ ID: destCol }).exec();

            const [movedCard] = srcColumn.cardList.splice(srcPos, 1);
            
            destColumn.cardList.splice(destPos, 0, movedCard);

            await srcColumn.save();
            await destColumn.save();
          }
          break;
        }
        case 'DELETE_CARD': {
          const { columnID, cardID } = message.data;

          await cardModel.findOneAndDelete({ ID: cardID }).exec();

          let targetColumn = await columnModel.findOne({ ID: columnID }).exec();
        
          const filteredList = targetColumn.cardList.filter(id => id !== cardID);
          targetColumn.cardList = filteredList;

          await targetColumn.save();
          break;
        }
        case 'UPDATE_CARD': {
          const { cardID, cardTitle = undefined, cardDescription = undefined, cardLabel = undefined, cardDeadline = undefined } = message.data;

          let targetCard = await cardModel.findOne({ ID: cardID }).exec();

          targetCard.title = cardTitle || targetCard.title;
          targetCard.description = cardDescription || targetCard.description;
          targetCard.label = cardLabel || targetCard.label;
          targetCard.deadline = cardDeadline || targetCard.deadline;
          
          await targetCard.save();
          break;
        }
        default:
          break;
      }

      clientList.forEach((client) => {
        if (client !== ws) {
          client.send(textMessage);
        }
      })
    })

    ws.on('close', function onClose() {
      console.log('Connection closed');
      clientList = clientList.filter(client => client !== ws);

      if (clientList.length === 0) {
        wss.close();
        console.log('Websocket server is closed');
        isServerRunning = false;
      }

    })
  })
}

module.exports = {
  InitializeServer
}