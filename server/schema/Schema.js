var mongoose = require('mongoose');
var MongooseSchema = mongoose.Schema;

var cardSchema = new MongooseSchema({
  ID: { type: String, required: true },
  title: { type: String, required: true },
  label: { type: Number, default: 0 },
  deadline: { type: Date, default: null },
  description: { type: String, default: '' }
});

var columnSchema = new MongooseSchema({
  ID: { type: String, required: true},
  title: { type: String, required: true },
  cardList: []
});

var columnListSchema = new MongooseSchema({
  wsChannel: { type: String, default: '' },
  list: []
});

module.exports = {
  columnListSchema,
  columnSchema,
  cardSchema
}
