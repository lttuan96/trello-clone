require('dotenv').config();

var mongoose = require('mongoose');

var dbAddress = 'mongodb://' + (process.env.DB_USERNAME && process.env.DB_PASSWORD ? process.env.DB_USERNAME + ':' + process.env.DB_PASSWORD
  + '@' : '') + process.env.DB_HOST + ':' + process.env.DB_PORT
  + '/' + process.env.DB_NAME;

mongoose.set('useFindAndModify', false);
mongoose.connect(dbAddress, { useNewUrlParser: true, useUnifiedTopology: true }, function (err) {
  if (err)
    console.log(err);
  else
    console.log('Database connected');
});

module.exports = mongoose;