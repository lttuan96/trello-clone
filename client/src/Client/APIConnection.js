import axios from 'axios';

const apiAddress = 'http://' + process.env.REACT_APP_API_HOST + ':' + process.env.REACT_APP_API_PORT;

axios.defaults.baseURL = apiAddress;

function requestServerData() {
  return axios.get('/')
}

export {
  requestServerData
}