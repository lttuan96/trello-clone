import { w3cwebsocket } from 'websocket';

function wsClient(path, store) {
  const wsAddress = 'ws://' + process.env.REACT_APP_WS_HOST + ':' + process.env.REACT_APP_WS_PORT + '/' + path;

  const client = new w3cwebsocket(wsAddress);

  client.onopen = () => {
    //Do something in future
  };

  client.onerror = function () {
    console.error('Connection Error');
  };

  client.onmessage = (msg) => {
    const action = JSON.parse(msg.data);

    store.dispatch({
      ...action,
      fromServer: true
    })
  };

  return client;
}

function wsMiddleware(client) {
  function handleReduxDispatch(action, wsClient) {
    if (action.fromServer === false) {
      switch (action.type) {
        case 'ADD_COLUMN':
        case 'MOVE_COLUMN':
        case 'DELETE_COLUMN':
        case 'RENAME_COLUMN':
        case 'ADD_CARD':
        case 'MOVE_CARD':
        case 'DELETE_CARD':
        case 'UPDATE_CARD':
          wsClient.send(JSON.stringify(action));
          return;
        default:
          return;
      }
    }
  }

  return ({getState}) => {
    return next => action => {
      const returnValue = next(action);
      
      handleReduxDispatch(action, client);
      
      return returnValue;
    }
  }
}

export {
  wsClient,
  wsMiddleware
};