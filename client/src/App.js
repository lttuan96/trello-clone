import React from "react";
import { connect } from 'react-redux';

import action from './Action/action';

import TaskColumn from "./Components/Column/column";
import AddColumn from './Components/AddColumn/addcolumn';
import DropSpace from './Components/DropSpace/dropspace';
import DeleteBin from './Components/DeleteBin/deletebin';

import "./App.css";

function App(props) {
  const { column, moveColumn } = props;

  const handleColumnMove = (data) => {
    const moveColumnData = action.getMoveColumnDispatchData(data.srcPos, data.destPos);
    if (moveColumnData === false) return;
    moveColumn(moveColumnData);
  }

  const lastDropColumnData = {
    id: column.list.length
  }

  const columnList = column.list.map((id, index) => {
    const dropColumnData = {
      id: index
    }
    return (
      <>
        <div className="ColumnDropContainer">
          <DropSpace
            acceptType="COLUMN"
            data={dropColumnData}
            onChange={handleColumnMove}
          />
        </div>
        <TaskColumn columnID={id} index={index} />
      </>
    );
  });

  const appComponent = (
    <div className="App">
      {columnList}
      <div className="ColumnDropContainer">
        <DropSpace
          acceptType="COLUMN"
          data={lastDropColumnData}
          onChange={handleColumnMove}
        />
      </div>

      <AddColumn />
      <DeleteBin />
    </div>
  );
  
  return appComponent;
}

const mapStateToProps = state => ({ column: state.column })

const mapDispatchToProps = (dispatch) => ({
  moveColumn: (data) =>
    dispatch({
      type: "MOVE_COLUMN",
      data: {
        srcPos: data.srcPos,
        destPos: data.destPos,
      },
      fromServer: false
    }),
});

export default connect(mapStateToProps, mapDispatchToProps)(App);
