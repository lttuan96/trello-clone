export const labelList = [
  {
    color: "#ffffff",
  },
  {
    color: "#6c57ff",
  },
  {
    color: "#40ff92",
  },
  {
    color: "#fe5f55",
  },
  {
    color: "#f7f24a",
  },
];