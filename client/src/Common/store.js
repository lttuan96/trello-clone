import { createStore, combineReducers, applyMiddleware } from 'redux';
import dynamicMiddlewares, { addMiddleware } from 'redux-dynamic-middlewares';
import { requestServerData } from '../Client/APIConnection';
import { wsClient, wsMiddleware } from '../Client/Websocket';

import action from '../Action/action';

// Using seed for testing
// import seed from './seed';

const column = (state = { list: []}, action) => {
  switch (action.type) {
    case 'ADD_COLUMN': {
      const { columnID } = action.data;

      return {
        ...state,
        list: [
          ...state.list,
          columnID
        ]
      };
    }

    case 'MOVE_COLUMN': {
      const { srcPos, destPos } = action.data;

      const [movedColumn] = state.list.splice(srcPos, 1);
      state.list.splice(destPos, 0, movedColumn);

      return {
        ...state,
        list: [
          ...state.list
        ]
      };
    }

    case 'DELETE_COLUMN': {
      const { columnID } = action.data;

      return {
        ...state,
        list: state.list.filter(id => id !== columnID)
      };
    }

    default:
      return state;
  }
}

const columnDetail = (state = {}, action) => {
  switch (action.type) {
    case 'ADD_CARD': {
      const { columnID, cardID } = action.data;

      return {
        ...state,
        [columnID]: {
          ...state[columnID],
          cardList: [
            ...state[columnID].cardList,
            cardID
          ]
        }
      };
    }

    case 'MOVE_CARD': {
      const { srcCol, destCol, srcPos, destPos } = action.data;

      const [movedCardID] = state[srcCol].cardList.splice(srcPos, 1);

      state[destCol].cardList.splice(destPos, 0, movedCardID);

      return {
        ...state,
        [srcCol]: {
          ...state[srcCol],
          'cardList': state[srcCol].cardList
        },
        [destCol]: {
          ...state[destCol],
          'cardList': state[destCol].cardList
        }
      };
    }

    case 'DELETE_CARD': {
      const { columnID, cardID } = action.data;

      return {
        ...state,
        [columnID]: {
          ...state[columnID],
          cardList: state[columnID].cardList.filter(id => id !== cardID),
        }
      };
    }

    
    case 'ADD_COLUMN': {
      const { columnID, columnTitle } = action.data;

      return {
        ...state,
        [columnID]: {
          'title': columnTitle,
          'cardList': []
        }
      };
    }

    case 'RENAME_COLUMN': {
      const { columnID, columnNewTitle } = action.data;

      return {
        ...state,
        [columnID]: {
          ...state[columnID],
          'title': columnNewTitle
        }
      };
    }

    case 'DELETE_COLUMN': {
      const { columnID } = action.data;

      delete state[columnID];

      return { ...state };
    }

    default:
      return state;
  }
}

const card = (state = {}, action) => {
  switch (action.type) {
    case 'ADD_CARD': {
      const { cardID, cardTitle, label = 0, description = '', deadline = '' } = action.data;

      return {
        ...state,
        [cardID]: {
          'title': cardTitle,
          'description': description,
          'label': label,
          'deadline': deadline
        }
      };
    }

    case 'UPDATE_CARD': {
      const { cardID, cardTitle = undefined, cardDescription = undefined, cardLabel = undefined, cardDeadline = undefined } = action.data;

      const { title, description, label, deadline } = state[cardID];

      return {
        ...state,
        [cardID]: {
          ...state[cardID],
          'title': cardTitle === undefined ? title : cardTitle,
          'description': cardDescription === undefined ? description : cardDescription,
          'label': cardLabel === undefined ? label : cardLabel,
          'deadline': cardDeadline === undefined ? deadline : cardDeadline,
        }
      };
    }
      
    case 'DELETE_CARD': {
      const { cardID } = action.data;

      delete state[cardID];

      return { ...state };
    }

    default:
      return state;
  }
}

const draggingItem = (state = { cardID: -1, type: "INIT" }, action) => {
  switch (action.type) {
    case 'UPDATE_DRAG':
      return action.data;
    default: return state;
  }
}

const dragFlag = (state = false, action) => {
  switch (action.type) {
    case 'TOGGLE_FLAG':
      return action.data;
    default: return state;
  }
}

const reducers = combineReducers({
  column,
  columnDetail,
  card,
  draggingItem,
  dragFlag
});

const store = createStore(reducers, applyMiddleware(dynamicMiddlewares));

requestServerData().then(response => {
  action.applyDataFromServer(response.data, store);
  
  const client = wsClient(response.data.wsChannel, store);

  addMiddleware(wsMiddleware(client))
});

export default store;