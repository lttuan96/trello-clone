import generateID from './generateID'

export default store => {
  const column1ID = generateID(8), column2ID = generateID(8), column3ID = generateID(8) ;

  store.dispatch({
    type: "ADD_COLUMN",
    data: {
      columnID: column1ID,
      columnTitle: 'Column 1'
    }
  })

  store.dispatch({
    type: "ADD_CARD",
    data: {
      columnID: column1ID,
      cardID: generateID(8),
      cardTitle: "Card 1 from Column 1",
      label: 2
    }
  })

  store.dispatch({
    type: "ADD_CARD",
    data: {
      columnID: column1ID,
      cardID: generateID(8),
      cardTitle: "Card 2 from Column 1",
      label: 1
    }
  })

  store.dispatch({
    type: "ADD_COLUMN",
    data: {
      columnID: column2ID,
      columnTitle: "Column 2"
    }
  })

  store.dispatch({
    type: "ADD_CARD",
    data: {
      columnID: column2ID,
      cardID: generateID(8),
      cardTitle: "Card 1 from Column 2",
      label: 3
    }
  })

  store.dispatch({
    type: "ADD_CARD",
    data: {
      columnID: column2ID,
      cardID: generateID(8),
      cardTitle: "Card 2 from Column 2",
      label: 4
    }
  })

  store.dispatch({
    type: "ADD_COLUMN",
    data: {
      columnID: column3ID,
      columnTitle: "Column 3"
    }
  })
}