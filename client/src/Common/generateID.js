export default function generateID(idLength) {
  let result = '', i;
  const characters = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz0123456789'
    , charactersLength = characters.length;

  for (i = 0; i < idLength; ++i) {
    result += characters[Math.floor(Math.random() * charactersLength)];
  }

  return result;
}