import React, {useState} from 'react';

import { connect } from 'react-redux';
import { FaPlus } from 'react-icons/fa';
import {IconContext} from 'react-icons'

import generateID from '../../Common/generateID';

import './addcolumn.css'

const dispatchType = 'ADD_COLUMN';

function AddColumn(props) {
  const [toggleInput, setToggleInput] = useState(false);

  const [inputValue, setInputValue] = useState('');

  const handleDisplayClick = () => {
    setToggleInput(true);
  }

  const handleInputChange = (e) => {
    setInputValue(e.target.value);
  }

  const handleCreate = () => {
    if (inputValue !== '') {
      props.addColumn(inputValue, generateID(12));
      setInputValue('');
    }
    setToggleInput(false);
  }

  const handleCancel = () => {
    setToggleInput(false);
  }

  const displayComponent = (
    <div className="AddColumnDisplayContainer" onClick={handleDisplayClick}>
      <IconContext.Provider value={{ className: "AddColumnIcon" }}>
        <FaPlus />
      </IconContext.Provider>
      NEW LIST
    </div>
  );

  const inputComponent = (
    <>
      <div className="AddColumnInputContainer">
        <input type="text" value={inputValue} onChange={handleInputChange} placeholder="Column title here..." autoFocus />
      </div>
      <div className="AddColumnButtonContainer">
        <button className="AddColumnBtn" onClick={handleCreate}>
          CREATE
        </button>
        <button className="CancelAddColumnBtn" onClick={handleCancel}>
          CANCEL
        </button>
      </div>
    </>
  );

  return (
    <div className="AddColumnContainer">
      {toggleInput ? inputComponent : displayComponent}
    </div>
  );
}

const mapDispatchToProps = (dispatch) => ({
  addColumn: (columnTitle, columnID) => dispatch({
    type: dispatchType, data: {
      columnID: columnID,
      columnTitle: columnTitle,
    },
    fromServer: false
  })
})

export default connect(null, mapDispatchToProps)(AddColumn);