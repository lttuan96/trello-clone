import React, {useState} from 'react'

import './detaildescription.css'

function DetailDescription(props) {
  const { description, onChange } = props;

  const [toggleInput, setToggleInput] = useState(false);

  const [inputValue, setInputValue] = useState(description);

  const handleDisplayClick = () => {
    setToggleInput(true);
  }

  const handleInputChange = (e) => {
    setInputValue(e.target.value);
  }

  const handleInputBlur = () => {
    onChange(inputValue);
    setToggleInput(false);
  }

  const InputComponent = (
    <textarea
      name="description"
      onBlur={handleInputBlur}
      onChange={handleInputChange}
      onFocus={(e) => e.target.select()}
      spellCheck={false}
      autoFocus>
      {inputValue}
    </textarea>
  );

  const DisplayComponent = (
    <a href="/#" onClick={handleDisplayClick}>
      {inputValue}
    </a>
  );

  const detailDescriptionComponent = (
    <div className="DescriptionContainer">
      {toggleInput ? InputComponent : DisplayComponent}
    </div>
  );

  return detailDescriptionComponent;
}

export default DetailDescription;