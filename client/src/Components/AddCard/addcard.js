import React, {useState} from 'react'

import {IconContext} from 'react-icons'
import { connect } from 'react-redux'
import { FaPlus} from 'react-icons/fa'

import generateID from '../../Common/generateID'

import './addcard.css'

const dispatchType = 'ADD_CARD';

function AddCard(props) {

  const [toggleInput, setToggleInput] = useState(false);

  const [inputValue, setInputValue] = useState();

  const handleAddCard = () => {
    setToggleInput(true);
  };

  const handleInputChange = (e) => {
    setInputValue(e.target.value);
  };

  const handleCreate = () => {
    if (inputValue !== undefined && inputValue !== '') {
      props.addCard(inputValue, props.columnID, generateID(12));
      setInputValue('');
    }

    setToggleInput(false);
  }

  const handleCancel = () => {
    setInputValue('');
    setToggleInput(false);
  }

  const addCardComponent = (
    <div className="AddCardContainer" onClick={handleAddCard}>
      <IconContext.Provider value={{ className: "AddIcon" }}>
        <FaPlus />
      </IconContext.Provider>
      ADD CARD
    </div>
  );

  const inputComponent = (
    <>
      <div className="InputContainer">
        <input
          autoFocus
          className="CardInput"
          type="text"
          placeholder="Card title here..."
          value={inputValue}
          onChange={handleInputChange}
        />
      </div>
      <div className="ButtonContainer">
        <button className="SaveBtn" onClick={handleCreate}>CREATE</button>
        <button className="CancelBtn" onClick={handleCancel}>CANCEL</button>
      </div>
    </>
  );

  return toggleInput ? inputComponent : addCardComponent;
}

const mapStatetoProps = (state, ownProps) => ({
  columnID: ownProps.columnID
})

const mapDispatchToProps = (dispatch) => ({
  addCard: (title, columnID, cardID) => dispatch({
    type: dispatchType, data: {
      cardID: cardID,
      columnID: columnID,
      cardTitle: title
    },
    fromServer: false
  })
})

export default connect(mapStatetoProps, mapDispatchToProps)(AddCard)