import React, { useState, useEffect } from "react";
import { FaPen } from "react-icons/fa";

import './texteditor.css'

function TextEditor(props) {

  const { value, onUpdate, isTitle } = props;

  const [toggleEdit, setToggleEdit] = useState(false);

  const [inputValue, setInputValue] = useState('');

  useEffect(() => {
    setInputValue(value)
  }, [value])

  const handleEdit = () => {
    setToggleEdit(true);
  };

  const handleOnBlur = () => {
    if (inputValue !== undefined && inputValue !== '') {
      onUpdate(inputValue);
    } else {
      setInputValue(value);
    }
    setToggleEdit(false);
  }

  const handleKeyDown = (e) => {
    if (e.key === "Enter") {
      if (inputValue !== undefined && inputValue !== '') {
        onUpdate(inputValue);
      } else {
        setInputValue(value);
      }
      setToggleEdit(false);
    }
  };

  const handleInputChange = (e) => {
    setInputValue(e.target.value);
  };

  const inputComponent = (
    <div style={{ position: "relative" }}>
      <input
        type="text"
        value={inputValue}
        className="TextInput"
        onBlur={handleOnBlur}
        onChange={handleInputChange}
        onKeyDown={handleKeyDown}
        autoFocus
      />
    </div>
  );

  const titleComponent = (
    <div style={{ position: "relative" }}>
      <h4 className="title">{inputValue}</h4>
      <button className="editBtn" onClick={handleEdit}>
        <FaPen />
      </button>
    </div>
  );

  const textComponent = (
    <div>
      <p>{inputValue}</p>
      <button className="editBtn" onClick={handleEdit}>
        <FaPen />
      </button>
    </div>
  );

  return toggleEdit ? inputComponent : isTitle ? titleComponent : textComponent;
}

export default TextEditor;
