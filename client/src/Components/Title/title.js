import React from "react";
import { connect } from "react-redux";

import TextEditor from "../TextEditor/texteditor";

import "./title.css";

function Title(props) {
  const { title, onTitleUpdate } = props;

  const handleTitleUpdate = (title) => {
    onTitleUpdate(title);
  };

  const titleComponent = (
    <div className="Title">
      <TextEditor isTitle={true} value={title} onUpdate={handleTitleUpdate} />
    </div>
  );

  return titleComponent;
}

export default connect()(Title);
