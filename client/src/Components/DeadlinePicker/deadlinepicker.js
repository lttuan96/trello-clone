import React, { useState } from 'react'

import DayPicker from 'react-day-picker'

import { IconContext } from 'react-icons'
import { FaTimes } from 'react-icons/fa'

import './deadlinepicker.css'
import 'react-day-picker/lib/style.css'


function DeadlinePicker(props) {
  const { deadline, onChange } = props;

  const timestampToString = deadline === "" ? deadline : new Date(deadline).toLocaleDateString();

  const [selectedDay, setSelectedDay] = useState(timestampToString);

  const [toggleDayPicker, setToggleDayPicker] = useState(false);

  const handleDeadlineChange = (newDate) => {
    let timestamp = newDate.getTime();

    onChange(timestamp);
    setSelectedDay(newDate.toLocaleDateString());
    setToggleDayPicker(!toggleDayPicker);
  }

  const handleCloseMenu = () => {
    setToggleDayPicker(false);
  }

  const handleToggleClick = () => {
    setToggleDayPicker(!toggleDayPicker);
  }

  const closeBtn = (
    <IconContext.Provider
      value={{ className: "CloseMenuBtn", size: "1.25em" }}
    >
      <FaTimes onClick={handleCloseMenu}/>
    </IconContext.Provider>
  );

  const CustomDayPicker = (
    <div class="DeadlinePickerContainer">
      <button className="DatePickerBtn" onClick={handleToggleClick}>
        {selectedDay === "" ? "Choose Deadline" : selectedDay}
      </button>
    </div>
  );

  return (
    <>
      {CustomDayPicker}
      {toggleDayPicker ? (
        <div className="DayPickerContanier">
          {closeBtn}
          <DayPicker onDayClick={handleDeadlineChange} />
        </div>
      ) : (
        ""
      )}
    </>
  );
  
}

export default DeadlinePicker;