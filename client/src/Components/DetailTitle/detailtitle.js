import React, {useState} from 'react'

import './detailtitle.css'


function DetailTitle(props) {
  const { title, onChange } = props;

  const [toggleEdit, setToggleEdit] = useState(false);

  const [detailInput, setDetailInput] = useState(title);

  const handleInputChange = (e) => {
    setDetailInput(e.target.value);
  }

  const handleInputBlur = () => {
    onChange(detailInput);
    setToggleEdit(false);
  }

  const handleInputKeyDown = (e) => {
    if (e.key === 'Enter') {
      onChange(detailInput);
      setToggleEdit(false);
    }
  }

  const handleDisplayClick = () => {
    setToggleEdit(true);
  }

  const inputComponent = (
    <input
      type="text"
      value={detailInput}
      onBlur={handleInputBlur}
      onChange={handleInputChange}
      onKeyDown={handleInputKeyDown}
      spellCheck={false}
      autoFocus
    />
  );

  const displayComponent = (
    <a href="/#" onClick={handleDisplayClick}>
      {detailInput}
    </a>
  );

  const detailTitleComponent = (
    <div className="TitleContainer">
      {toggleEdit ? inputComponent : displayComponent}
    </div>
  );

  return detailTitleComponent;
}

export default DetailTitle;