import React, { useState } from "react";
import { connect } from "react-redux";
import { useDrag } from "react-dnd";

import CardDetail from "../CardDetail/carddetail";
import { labelList } from "../../Common/label.js";

import "./card.css";

const dispatchTypes = {
  updatedrag: "UPDATE_DRAG",
  updatedetail: "UPDATE_CARD",
  updatedragflag: "TOGGLE_FLAG"
}

function TaskCard(props) {
  const [toggleDialog, setToggleDialog] = useState(false);

  const { cardDetail, cardID, index, columnID, updateDrag, updateDragFlag, updateDetail } = props;

  const { title, label } = cardDetail;

  const { color } = labelList[label];

  const itemTypes = {
    type: "CARD",
    id: cardID,
    index: index,
    columnID: columnID,
  };

  const [{ isDragging }, drag] = useDrag({
    item: itemTypes,
    collect: (monitor) => {
      const item = monitor.getItem();
      if (item !== null) {
        updateDrag(item);
        updateDragFlag(true);
      }

      return {
        isDragging: !!monitor.isDragging(),
      };
    },
    end: () => updateDragFlag(false)
  });

  const onCardClick = () => {
    setToggleDialog(true);
  };

  const handleDetailUpdate = (detail) => {
    updateDetail(cardID, detail);
  }

  return (
    <>
      <div
        ref={drag}
        className="TaskCard"
        onClick={onCardClick}
        style={{ opacity: isDragging ? 0 : 1 }}
      >
        <div
          className="CardLabel"
          style={{
            backgroundColor: color,
          }}
        ></div>
        {title}
      </div>

      {toggleDialog ? (
        <CardDetail
          detail={cardDetail}
          isOpen={toggleDialog}
          setOpen={setToggleDialog}
          onUpdate={handleDetailUpdate}
        />
      ) : null}
    </>
  );
}

const mapStateToProps = (state, ownProps) => ({
  cardDetail: state.card[ownProps.cardID],
  cardID: ownProps.cardID,
  index: ownProps.index,
  currentDrag: state.drag,
  columnID: ownProps.columnID,
});

const mapDispatchToProps = (dispatch) => ({
  updateDrag: (data) =>
    dispatch({
      type: dispatchTypes.updatedrag, data: data
    }),
  
  updateDetail: (cardID, { title, description, deadline, label }) =>
    dispatch({
      type: dispatchTypes.updatedetail,
      data: {
        cardID: cardID,
        cardTitle: title,
        cardDescription: description,
        cardLabel: label,
        cardDeadline: deadline,
      },
      fromServer: false
    }),
  
  updateDragFlag: (value) =>
    dispatch({
      type: dispatchTypes.updatedragflag,
      data: value
    }),
});

export default connect(mapStateToProps, mapDispatchToProps)(TaskCard);
