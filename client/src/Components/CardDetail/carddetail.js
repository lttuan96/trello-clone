import React, { useEffect, useRef } from 'react'

import DeadlinePicker from "../DeadlinePicker/deadlinepicker";
import LabelPicker from '../LabelPicker/labelpicker';
import DetailTitle from '../DetailTitle/detailtitle';
import DetailDescription from '../DetailDescription/detaildescription';

import './carddetail.css'

function CardDetail(props) {
  const { onUpdate, detail, setOpen, isOpen } = props;

  const { title, description, label, deadline } = detail;

  const outsideRef = useRef();

  useEffect(() => {
    function handleOutsideClick(event) {
      const { target } = event;

      if (outsideRef.current.contains(target) && isOpen) {
        setOpen(false);
      }
    }

    document.addEventListener("click", handleOutsideClick);

    return () => {
      document.removeEventListener("click", handleOutsideClick);
    };
  }, [isOpen, setOpen]);

  const handleLabelChange = (newColor) => {
    onUpdate({
      label: newColor
    })
  } 
  
  const handleTitleChange = (newTitle) => { 
    onUpdate({
      title: newTitle
    })
  }

  const handleDescriptionChange = (newDescription) => {
    onUpdate({
      description: newDescription
    })
  }

  const handleDeadlineChange = (newDeadline) => {
    onUpdate({
      deadline: newDeadline
    })
  }

  return (
    <>
      <div className="Outside" ref={outsideRef} />

      <div className="CardDetail">
        <div className="DetailLabel">
          <LabelPicker label={label} onChange={handleLabelChange} />
        </div>
        
        <div className="DetailTitle">
          <DetailTitle title={title} onChange={handleTitleChange} />
        </div>

        <div className="DetailDescription">
          <DetailDescription description={description} onChange={handleDescriptionChange}  />
        </div>

        <div className="DetailDeadline">
          <DeadlinePicker deadline={deadline} onChange={handleDeadlineChange} />
        </div>
      </div>
    </>
  );
}

export default CardDetail;