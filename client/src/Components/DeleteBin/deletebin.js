import React, {useRef, useEffect, useState} from 'react';

import { FaTrashAlt } from 'react-icons/fa';
import { IconContext } from 'react-icons';
import { useDrop } from 'react-dnd';
import { connect } from 'react-redux';

import './deletebin.css';

const acceptTypes = ['CARD', 'COLUMN'];

const dispatchTypes = {
  deletecard: "DELETE_CARD",
  deletecolumn: "DELETE_COLUMN"
};

function DeleteBin(props) {
  const {
    deleteCard,
    deleteColumn,
    dragFlag,
    dragItem,
    columnDetail,
    column,
  } = props;

  const [toggleDialog, setToggleDialog] = useState(false);
  const [toggleErrorDialog, setToggleErrorDialog] = useState(false);

  const outsideBoxRef = useRef();

  const handleConfirmDelete = () => {
    switch (dragItem.type) {
      case "CARD":
        deleteCard(dragItem.columnID, dragItem.id);
        break;
      case "COLUMN":
        if (columnDetail[column.list[dragItem.id]].cardList.length === 0) {
          deleteColumn(column.list[dragItem.id]);
        } else {
          setToggleErrorDialog(true);
        }
        
        break;
      default:
        return;
    }
    setToggleDialog(false);
  }

  const handleCancelDelete = () => {
    setToggleDialog(false);
  }

  const handleConfirmError = () => {
    setToggleErrorDialog(false);
  }

  useEffect(() => {
    function handleOutsideClick(event) {
      const { target } = event;

      if (!outsideBoxRef.current) {
        return;
      }

      if (
        (toggleDialog || toggleErrorDialog) &&
        outsideBoxRef.current.contains(target)
      ) {
        if (toggleDialog) {
          setToggleDialog(false);
        }

        if (toggleErrorDialog) {
          setToggleErrorDialog(false);
        }
      }
    }
    document.addEventListener("click", handleOutsideClick);

    return () => {
      document.removeEventListener("click", handleOutsideClick);
    };
  }, [toggleDialog, setToggleDialog, toggleErrorDialog, setToggleErrorDialog]);

  const [{ isOver }, drop] = useDrop({
    accept: acceptTypes,
    collect: (monitor) => ({
      isOver: monitor.isOver(),
    }),
    drop: () => setToggleDialog(true),
  });

  const confirmBox = (
    <div className="DialogBox">
      <p>Delete this?</p>
      <div className="ConfirmBtnContainer">
        <button onClick={handleConfirmDelete}>YES</button>
        <button onClick={handleCancelDelete}>NO</button>
      </div>
    </div>
  );

  const deleteErrorBox = (
    <div className="DialogBox">
      <p>Cannot delete non-empty list</p>
      <div className="ErrorBtnContainer">
        <button onClick={handleConfirmError}>OK</button>
      </div>
    </div>
  )

  const dropBin = (
    <div
      className="DropBin"
      ref={drop}
      style={{ backgroundColor: isOver === true ? "#FFD700" : "#FFFFFF" }}
    >
      <IconContext.Provider value={{ size: "1.5em", color: "#DC143C" }}>
        <FaTrashAlt />
      </IconContext.Provider>
    </div>
  );

  const deleteBinComponent = (
    <div>
      {dragFlag ? dropBin : ""}
      {toggleDialog ? confirmBox : ""}
      {toggleErrorDialog ? deleteErrorBox : ""}
      {toggleDialog || toggleErrorDialog ? (
        <div className="OutsideBox" ref={outsideBoxRef} />
      ) : (
          ""
        )}
    </div>
  );

  return deleteBinComponent;
}

const mapStateToProps = (state, ownProps) => ({
  dragFlag: state.dragFlag,
  dragItem: state.draggingItem,
  columnDetail: state.columnDetail,
  column: state.column,
});

const mapDispatchToProps = (dispatch) => ({
  deleteCard: (columnID, cardID) => dispatch({
    type: dispatchTypes.deletecard,
    data: {
      columnID: columnID,
      cardID: cardID,
    },
    fromServer: false
  }),
  deleteColumn: (columnID) => dispatch({
    type: dispatchTypes.deletecolumn,
    data: {
      columnID: columnID,
    },
    fromServer: false
  })
})

export default connect(mapStateToProps, mapDispatchToProps)(DeleteBin);