import React from 'react';

import { connect } from 'react-redux';
import { useDrop } from 'react-dnd';

import './dropspace.css';

function DropSpace(props) {

  const { acceptType, onChange, data, currentDrag } = props;
  
  const handleOnDrop = (dropItem) => {
    switch (acceptType) {
      case "CARD":
        const cardCallback = {
          id: data.id,
          index: data.index,
          sourceIndex: dropItem.index,
          sourceID: dropItem.columnID,
        }

        onChange(cardCallback);
        return;
      case "COLUMN":
        const columnCallback = {
          srcPos: dropItem.id,
          destPos: data.id
        }
        
        onChange(columnCallback);
        return;

      default:
        return;
    }
  }

   const [{ isOver }, drop] = useDrop({
     accept: acceptType,
     collect: (monitor) => ({
       isOver: !!monitor.isOver(),
     }),
     drop: (item) => handleOnDrop(item),
   });
  
  const dropSpaceComponent = (
    <div
      className="Space"
      ref={drop}
      style={{
        backgroundColor:
          currentDrag.type === acceptType &&
          isOver &&
          currentDrag.id !== data.id
            ? "#bcbfbb"
            : "#ffffff",
      }}
    >
      
    </div>
  );

  return dropSpaceComponent;
}

const mapStateToProps = (state, ownProps) => ({
  acceptType: ownProps.acceptType,
  onChange: ownProps.onChange,
  data: ownProps.data,
  currentDrag: state.draggingItem,
})

export default connect(mapStateToProps)(DropSpace);