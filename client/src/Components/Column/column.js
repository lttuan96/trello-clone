import React from "react";
import { connect } from "react-redux";
import { useDrag } from 'react-dnd';

import Title from "../Title/title";
import TaskCard from "../Card/card";
import DropSpace from "../DropSpace/dropspace";
import AddCard from "../AddCard/addcard";

import action from "../../Action/action";

import "./column.css";

const dispatchTypes = {
  rename: "RENAME_COLUMN",
  movecard: "MOVE_CARD",
  updatedrag: "UPDATE_DRAG",
  updatedragflag: "TOGGLE_FLAG"
}

function TaskColumn(props) {
  const { columnDetail, columnID, columnIndex, updateDrag, updateDragFlag, moveCard, renameColumn } = props;

  const dragItemInfo = {
    type: "COLUMN",
    id: columnIndex,
  };

  const [{ isDragging }, drag] = useDrag({
    item: dragItemInfo,
    collect: (monitor) => {
      const item = monitor.getItem();
      if (item !== null) {
        updateDrag(item);
        updateDragFlag(true);
      }

      return {
        isDragging: !!monitor.isDragging(),
      };
    },
    end: () => updateDragFlag(false)
  });

  const DropSpaceData = {
    id: columnID,
    index: columnDetail.cardList.length,
  }

  const handleCardMove = (data) => {
    moveCard(data);
  }

  const handleTitleUpdate = (newTitle) => {
    renameColumn(columnID, newTitle);
  }

  const columnDragBar = (
    <div className="ColumnDragBar">
      <hr></hr>
    </div>
  );

  const outterDropSpace = (
    <div className="DropSpaceContainer" >
      <DropSpace
        data={DropSpaceData}
        onChange={handleCardMove}
        acceptType="CARD"
      />
    </div >
  );

  const CardList = columnDetail.cardList.map((id, cardIndex) => {
    const dropCardSpaceData = {
      id: columnID,
      index: cardIndex,
    }

    return (
      <>
        <div className="DropSpaceContainer">
          <DropSpace data={dropCardSpaceData} acceptType="CARD" onChange={handleCardMove} />
        </div>
        <TaskCard cardID={id} columnID={columnID} index={cardIndex} />
      </>
    )
  });

  return (
    <div className="TaskColumnContainer" ref={drag} style={{opacity: isDragging? 0 : 1}}>
      {columnDragBar}
      <Title
        title={columnDetail.title}
        onTitleUpdate={handleTitleUpdate}
      ></Title>
      {CardList}
      {outterDropSpace}
      <AddCard columnID={columnID} />
    </div>
  );
}

const mapStateToProps = (state, ownProps) => ({
  columnDetail: state.columnDetail[ownProps.columnID],
  columnID: ownProps.columnID,
  columnIndex: ownProps.index,
});

const mapDispatchToProps = (dispatch) => ({
  renameColumn: (columnID, newColumnTitle) =>
    dispatch({
      type: dispatchTypes.rename,
      data: {
        columnID: columnID,
        columnNewTitle: newColumnTitle,
      },
      fromServer: false
    }),
  
  moveCard: (data) =>
    dispatch({
      type: dispatchTypes.movecard,
      data:action.getMoveCardDispatchData(
          data.sourceIndex,
          data.index,
          data.sourceID,
          data.id
      ),
      fromServer: false
    }),
  
  updateDrag: (data) =>
    dispatch({
      type: dispatchTypes.updatedrag,
      data: data,
    }),
  
  updateDragFlag: (value) =>
    dispatch({
      type: dispatchTypes.updatedragflag,
      data: value
    })
});

export default connect(mapStateToProps, mapDispatchToProps)(TaskColumn);
