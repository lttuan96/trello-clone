import React, {useState} from 'react';

import {IconContext} from 'react-icons'
import { FaArrowRight, FaTimes } from 'react-icons/fa';

import DetailColorPicker from '../DetalColorPicker/detailcolorpicker'
import { labelList } from "../../Common/label.js";

import './labelpicker.css'

function LabelPicker(props) {
  const { label, onChange } = props;

  const [togglePicker, setTogglePicker] = useState(false);

  const color = labelList[label].color;

  const handleOnClick = () => {
    setTogglePicker(!togglePicker)
  }

  const handleLabelChange = (color) => {
    onChange(color);
  }

  const handleCloseMenu = () => {
    setTogglePicker(false)
  }

  const colorButtonComponent = (
    <div className="ColorContainer">
        <div style={{ backgroundColor: color }} className="ColorField"></div>
        <button className="ColorBtn" onClick={handleOnClick}>
          <IconContext.Provider
            value={{ className: "ColorIcon", size: "1.25em" }}
          >
            {togglePicker === true ? <FaTimes /> : <FaArrowRight />}
          </IconContext.Provider>
        </button>
        {togglePicker === true ?
          <DetailColorPicker
          label={label}
          labelList={labelList}
          onChange={handleLabelChange}
          onClose={handleCloseMenu}
        /> : ''}
      </div>
  );
  
  return colorButtonComponent;
}


export default LabelPicker;