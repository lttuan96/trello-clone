import React from 'react';

import {IconContext} from 'react-icons'
import {FaCheck, FaTimes} from 'react-icons/fa'

import './detailcolorpicker.css'

function DetailColorPicker(props) {
  const { label, labelList, onChange, onClose } = props;

  const handleColorPick = (color) => {
    onChange(color);
  }

  const handleCloseMenu = () => {
    onClose();
  };

  const colorListComponent = labelList.map((item, colorIndex) => {
    return (
      <div
        className="ColorItem"
        onClick={() => handleColorPick(colorIndex)}
        style={{ backgroundColor: item.color }}
      >
        {colorIndex === label ? (
          <IconContext.Provider value={{ className: "SelectedColor" }}>
            <FaCheck />
          </IconContext.Provider>
        ) : (
          ""
        )}
      </div>
    );
  });

  const closeBtn = (
    <IconContext.Provider value={{ className: "CloseMenuBtn" }}>
      <FaTimes onClick={handleCloseMenu}/>
    </IconContext.Provider>
  );

  const colorPickerComponent = (
    <div className="ColorPickerContainer">
      {closeBtn}
      <p>Choose Color</p>
      {colorListComponent}
    </div>
  );

  return colorPickerComponent;
}

export default DetailColorPicker;