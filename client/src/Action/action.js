function getMoveCardDispatchData(srcPos, destPos, srcCol, destCol) {
  return srcCol === destCol && srcPos < destPos ? {
    srcPos: srcPos,
    destPos: destPos - 1,
    srcCol: srcCol,
    destCol: destCol
  } :
    {
      srcPos: srcPos,
      destPos: destPos,
      srcCol: srcCol,
      destCol: destCol
    }
}

function getMoveColumnDispatchData(srcPos, destPos) {
  if (srcPos === destPos || srcPos + 1 === destPos) return false;

  if (srcPos < destPos) {
    return {
      srcPos: srcPos,
      destPos: destPos - 1
    }
  }

  if (srcPos > destPos) {
    return {
      srcPos: srcPos,
      destPos: destPos
    }
  }
}

function applyDataFromServer(data, store) {
  let i, j;
  const { list, columnList, cardList } = data;

  function findItemByIdInArray(array, item) {
    const [result] = array.filter(value => value.ID === item);

    return result;
  }

  for (i = 0; i < list.length; ++i) {
    const columnDetail = findItemByIdInArray(columnList, list[i]);
    const dispatchCards = columnDetail.cardList;

    store.dispatch({
      type: 'ADD_COLUMN',
      data: {
        columnID: list[i],
        columnTitle: columnDetail.title
      },
      fromServer: true
    })

    for (j = 0; j < dispatchCards.length; ++j) {
      const cardDetail = findItemByIdInArray(cardList, dispatchCards[j]);
      
      store.dispatch({
        type: 'ADD_CARD',
        data: {
          columnID: columnDetail.ID,
          cardID: cardDetail.ID,
          cardTitle: cardDetail.title,
          label: cardDetail.label,
          deadline: cardDetail.deadline,
          description: cardDetail.description,
        },
        fromServer: true
      })
    }
  }
}

export default {
  getMoveCardDispatchData,
  getMoveColumnDispatchData,
  applyDataFromServer
}