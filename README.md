# Trello Clone
Small project comprises of server, client and database.

* Feature: multiple clients can work at the same time using websocket.

# Setting up server

* Create .env file using template from .env-example (DB_USERNAME and DB_PASSWORD is nullable).
* Get the server running by typing node app.js or npm run start in terminal at server directory.

# Client

* Create .env file using template from .env-example. Then type npm run build in terminal at root directory.
* You can use the already built product inside /build directory if you use default setting in .env.
* Open index.html.